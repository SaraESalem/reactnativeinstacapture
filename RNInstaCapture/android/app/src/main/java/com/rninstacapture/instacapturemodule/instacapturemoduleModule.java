//  Created by react-native-create-bridge

package com.rninstacapture.instacapturemodule;
import android.widget.Toast;
import android.support.annotation.Nullable;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.uimanager.IllegalViewOperationException;

import android.graphics.Bitmap;
import android.util.Base64;
import java.io.*;
import com.tarek360.instacapture.screenshot.*;
import com.tarek360.instacapture.*;
import com.tarek360.instacapture.listener.SimpleScreenCapturingListener;


import java.util.HashMap;
import java.util.Map;

public class instacapturemoduleModule extends ReactContextBaseJavaModule {
    public static final String REACT_CLASS = "instacapturemodule";
    private static ReactApplicationContext reactContext = null;

    public instacapturemoduleModule(ReactApplicationContext context) {
        // Pass in the context to the constructor and save it so you can emit events
        // https://facebook.github.io/react-native/docs/native-modules-android.html#the-toast-module
        super(context);

        reactContext = context;
        Instacapture.INSTANCE.enableLogging(true);
    }

    @Override
    public String getName() {
        // Tell React the name of the module
        // https://facebook.github.io/react-native/docs/native-modules-android.html#the-toast-module
        return REACT_CLASS;
    }

    @Override
    public Map<String, Object> getConstants() {
        // Export any constants to be used in your native module
        // https://facebook.github.io/react-native/docs/native-modules-android.html#the-toast-module
        final Map<String, Object> constants = new HashMap<>();
        constants.put("EXAMPLE_CONSTANT", "example");

        return constants;
    }

    @ReactMethod
    public void welcomeToast (String msgx) {
        String msg;
        if(msgx != null && !msgx.isEmpty()) { 
            msg = msgx;
        }else{
            msg = "Nothing to show";
        }
         Toast.makeText(getReactApplicationContext(), msgx, Toast.LENGTH_LONG).show();
        
    }

    @ReactMethod
    public void captureScreen (final Callback errorCallback,final Callback successCallback) {
        System.out.println("**************************************************");
        try {
         Instacapture.INSTANCE.capture(getCurrentActivity(), new SimpleScreenCapturingListener() {
            @Override
            public void onCaptureComplete(Bitmap bitmap) {
            //Your code here..
                try {
                    System.out.println("************************bitmap**************************");
//                    System.out.println(bitmap);
                    String bsae64Img = convertBitmapToBase64(bitmap);
                    System.out.println("************************bsae64Img**************************");
                    System.out.println(bsae64Img);
                    successCallback.invoke(bsae64Img);
                } catch (IllegalViewOperationException e) {
                    errorCallback.invoke(e.getMessage());
                }    
            }
        });
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }


    //CONVERT THE BITMAP IN TO BASE64 STRING
    public static String convertBitmapToBase64(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
 
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private static void emitDeviceEvent(String eventName, @Nullable WritableMap eventData) {
        // A method for emitting from the native side to JS
        // https://facebook.github.io/react-native/docs/native-modules-android.html#sending-events-to-javascript
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, eventData);
    }
}
