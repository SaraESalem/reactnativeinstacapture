//  Created by react-native-create-bridge

import { NativeModules } from 'react-native'

const { instacapturemodule} = NativeModules

export default {
  welcomeToast (msg) {
    return instacapturemodule.welcomeToast(msg)
  },
  captureScreen(errorCallback,successCallback) {
    return instacapturemodule.captureScreen(errorCallback,successCallback)
  },

  EXAMPLE_CONSTANT: instacapturemodule.EXAMPLE_CONSTANT
}
