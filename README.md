# ReactNativeInstaCapture

a React Native module for Instacapture - Android: used to capture a screenshot of all the current content on a screen.

#install and creation

react-native init RNInstaCapture
react-native run-android

#Module Folder

RNInstaCapture/android/app/src/main/java/com/rninstacapture/instacapturemodule

installed by react-native-create-bridge

#apk
in root folder

#API USED 
Instacapture 2.0

